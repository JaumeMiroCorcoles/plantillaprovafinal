package es.cipfpbatoi.plantillaexamen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PlantillaExamenApplication {

    public static void main(String[] args) {
        SpringApplication.run(PlantillaExamenApplication.class, args);
    }

}
